#!/bin/bash

[ -f group_environment.sh ] && source group_environment.sh
[ -f environment.sh ] && source environment.sh
[ -f version.sh ] && source version.sh

image_version="${image_version:-$(curl -s https://api.github.com/repos/linuxserver/docker-transmission/releases | grep tag_name | grep -v -- '-rc' | head -1 | awk -F': ' '{print $2}' | sed 's/,//' | xargs)}"

branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')

if [ "$branch" == "main" ] || [ "$branch" == "(HEAD detached at origin/main)" ]; then
    export deploy_env="prd"
    ip_subnet="34"
elif [ "$branch" == "staging" ] || [ "$branch" == "(HEAD detached at origin/staging)" ]; then
    export deploy_env="qua"
    ip_subnet="33"
else
    export deploy_env="dev"
    ip_subnet="32"
fi

work=$(mktemp -d)
trap 'rm -rf -- "${work}"' EXIT
base=$(realpath --relative-to=${work} $PWD)

cat << EOF > ${work}/deployment.yml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-transmission-config
  labels:
    app: transmission
  namespace: media
spec:
  storageClassName: rook-ceph-block
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: ${config_size}Gi
---      
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: transmission
  name: transmission
  namespace: media-services
spec:
  replicas: 1
  selector:
    matchLabels:
      app: transmission
  template:
    metadata:
      labels:
        app: transmission
    spec:
      containers:
      - image: linuxserver/transmission:${image_version}
        name: transmission
        volumeMounts:
        - name: transmission-config
          mountPath: /config/
        - name: transmission-downloads
          mountPath: /downloads/
        env:
          name: PUID
          value: ${puid}
          name: PGID
          value: ${pgid}
          name: TZ
          value: ${tz}
          name: TRANSMISSION_WEB_HOME
          value: ${transmission_web_home}
      volumes:
      - name: transmission-config
        persistentVolumeClaim:
          claimName: pvc-transmission-config
      - name: transmission-downloads
        persistentVolumeClaim:
          claimName: pvc-transmission-downloads
---
apiVersion: v1
kind: Service
metadata:
  name: transmission
  namespace: media
  annotations:
    metallb.universe.tf/allow-shared-ip: "key-to-share-${base_ip}.${ip_subnet}.${loadbal_ip}"
spec:
  selector:
    app: transmission
  allocateLoadBalancerNodePorts: false
  ports:
    - name: transmission
      port: 9091
      targetPort: 9091
  type: LoadBalancer
  loadBalancerIP: ${base_ip}.${ip_subnet}.${loadbal_ip}
EOF

ls -al ${work}
cd ${work}

cat deployment.yml